﻿using System;

namespace TvMazeScrapper.Models.App
{
    public class PersonModel
    {
        public DateTime? Birthday { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
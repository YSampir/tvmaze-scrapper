﻿namespace TvMazeScrapper.Domain.TvMaze
{
    public class TvMazeShow
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
﻿using System;

namespace TvMazeScrapper.Services.Api.TvMazeApi.DataModels
{
    public class PersonData
    {
        public DateTime? Birthday { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
    }
}